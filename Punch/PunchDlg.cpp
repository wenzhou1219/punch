
// PunchDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "resource.h"
#include "Punch.h"
#include "PunchDlg.h"
#include "HelpDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define WM_NOTIFYICON WM_USER+1
#define TIMER_1 1

// 用于应用程序“关于”菜单项的 CAboutDlg 对话框

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// 对话框数据
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnNMClickWebsiteLink(NMHDR *pNMHDR, LRESULT *pResult);
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()

//添加网址链接
void CAboutDlg::OnNMClickWebsiteLink(NMHDR *pNMHDR, LRESULT *pResult)
{
	// TODO: 在此添加控件通知处理程序代码
	*pResult = 0;
	PNMLINK pNMLink = (PNMLINK)pNMHDR;
	::ShellExecute(m_hWnd, _T("open"), pNMLink->item.szUrl, NULL, NULL, SW_SHOWNORMAL);
	*pResult = 0;
}


// CPunchDlg 对话框
CPunchDlg::CPunchDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CPunchDlg::IDD, pParent)
	, m_bMornEnable(FALSE)
	, m_timeMorn(0)
	, m_nMornSpace(0)
	, m_bNoonEnable(FALSE)
	, m_timeNoon(0)
	, m_nNoonSpace(0)
	, m_bAfterEnable(FALSE)
	, m_timeAfter(0)
	, m_nAfterSpace(0)
	, m_bNightEnable(FALSE)
	, m_timeNight(0)
	, m_nNightSpace(0)
	, m_bAutoRun(FALSE)
	, m_bStartNotify(FALSE)
	, m_bEndNotify(FALSE)
	, m_bHide(FALSE)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);

	m_bIsToTray = FALSE;//最开始没有托盘化
}

void CPunchDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Check(pDX, IDB_MORNING_ENABLE, m_bMornEnable);
	DDX_DateTimeCtrl(pDX, IDB_MORNING_TIME, m_timeMorn);
	DDX_Text(pDX, IDB_MORNING_SPACE, m_nMornSpace);
	DDV_MinMaxUInt(pDX, m_nMornSpace, 0, 60);
	DDX_Check(pDX, IDB_NOON_ENABLE, m_bNoonEnable);
	DDX_DateTimeCtrl(pDX, IDB_NOON_TIME, m_timeNoon);
	DDX_Text(pDX, IDB_NOON_SPACE, m_nNoonSpace);
	DDV_MinMaxUInt(pDX, m_nNoonSpace, 0, 60);
	DDX_Check(pDX, IDB_AFTERNOON_ENABLE, m_bAfterEnable);
	DDX_DateTimeCtrl(pDX, IDB_AFTERNOON_TIME, m_timeAfter);
	DDX_Text(pDX, IDB_AFTERNOON_SPACE, m_nAfterSpace);
	DDV_MinMaxUInt(pDX, m_nAfterSpace, 0, 60);
	DDX_Check(pDX, IDB_NIGHT_ENABLE, m_bNightEnable);
	DDX_DateTimeCtrl(pDX, IDB_NIGHT_TIME, m_timeNight);
	DDX_Text(pDX, IDB_NIGHT_SPACE, m_nNightSpace);
	DDV_MinMaxUInt(pDX, m_nNightSpace, 0, 60);
	DDX_Check(pDX, IDC_AUTORUN, m_bAutoRun);
	DDX_Check(pDX, IDC_START_NOTIFY, m_bStartNotify);
	DDX_Check(pDX, IDC_END_NOTIFY, m_bEndNotify);
	DDX_Control(pDX, IDC_HOTKEY, m_HotKeyCtrl);
	DDX_Check(pDX, IDB_HIDE, m_bHide);
}

BEGIN_MESSAGE_MAP(CPunchDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_WM_DESTROY()
	ON_MESSAGE (WM_NOTIFYICON ,&OnNotifyIcon)
	ON_COMMAND(IDM_SETTING, &CPunchDlg::OnSetting)
	ON_COMMAND(IDM_HELP, &CPunchDlg::OnHelp)
	ON_COMMAND(IDM_ABOUT, &CPunchDlg::OnAbout)
	ON_COMMAND(IDM_EXIT, &CPunchDlg::OnExit)
	ON_BN_CLICKED(IDOK, &CPunchDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CPunchDlg::OnBnClickedCancel)
	ON_WM_NCPAINT()
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_START_NOTIFY, &CPunchDlg::OnBnClickedStartNotify)
	ON_BN_CLICKED(IDC_AUTORUN, &CPunchDlg::OnBnClickedAutorun)
	ON_WM_HOTKEY()
	ON_WM_QUERYENDSESSION()
END_MESSAGE_MAP()


// CPunchDlg 消息处理程序

BOOL CPunchDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);


	// 设置此对话框的图标。当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// TODO: 在此添加额外的初始化代码

	/***初始化读取参数和设置参数,对于部分参数需要处理一下得到对应的界面参数和内存参数***/
	m_configIni = CIni(theApp.m_csConfigFileName.GetBuffer(MAX_PATH));
	CTime timeCurrent = CTime::GetCurrentTime();

	m_bMornEnable = m_configIni.GetInt(TEXT("Morning"), TEXT("Enable"), 2);
	m_timeMornAct = m_timeMorn = CTime(timeCurrent.GetYear(), timeCurrent.GetMonth(), timeCurrent.GetDay(), m_configIni.GetInt(TEXT("Morning"), TEXT("Hour"), 7), m_configIni.GetInt(TEXT("Morning"), TEXT("Minute"), 50), m_configIni.GetInt(TEXT("Morning"), TEXT("Second"), 0), 0);
	m_nMornSpace = m_configIni.GetInt(TEXT("Morning"), TEXT("Space"), 5);

	m_bNoonEnable = m_configIni.GetInt(TEXT("Noon"), TEXT("Enable"), 2);
	m_timeNoonAct = m_timeNoon = CTime(timeCurrent.GetYear(), timeCurrent.GetMonth(), timeCurrent.GetDay(), m_configIni.GetInt(TEXT("Noon"), TEXT("Hour"), 11), m_configIni.GetInt(TEXT("Noon"), TEXT("Minute"), 50), m_configIni.GetInt(TEXT("Noon"), TEXT("Second"), 0), 0);
	m_nNoonSpace = m_configIni.GetInt(TEXT("Noon"), TEXT("Space"), 5);

	m_bAfterEnable = m_configIni.GetInt(TEXT("Afternoon"), TEXT("Enable"), 2);
	m_timeAfterAct = m_timeAfter = CTime(timeCurrent.GetYear(), timeCurrent.GetMonth(), timeCurrent.GetDay(), m_configIni.GetInt(TEXT("Afternoon"), TEXT("Hour"), 2), m_configIni.GetInt(TEXT("Afternoon"), TEXT("Minute"), 30), m_configIni.GetInt(TEXT("Afternoon"), TEXT("Second"), 0), 0);
	m_nAfterSpace = m_configIni.GetInt(TEXT("Afternoon"), TEXT("Space"), 5);

	m_bNightEnable = m_configIni.GetInt(TEXT("Night"), TEXT("Enable"), 2);
	m_timeNightAct = m_timeNight = CTime(timeCurrent.GetYear(), timeCurrent.GetMonth(), timeCurrent.GetDay(), m_configIni.GetInt(TEXT("Night"), TEXT("Hour"), 6), m_configIni.GetInt(TEXT("Night"), TEXT("Minute"), 0), m_configIni.GetInt(TEXT("Night"), TEXT("Second"), 0), 0);
	m_nNightSpace = m_configIni.GetInt(TEXT("Night"), TEXT("Space"), 5);

	m_bAutoRun = m_configIni.GetInt(TEXT("Other"), TEXT("Autorun"), 1);
	m_bStartNotify = m_configIni.GetInt(TEXT("Other"), TEXT("StartNotify"), 1);
	m_bEndNotify = m_configIni.GetInt(TEXT("Other"), TEXT("EndNotify"), 1);

	m_bHotKey_Shift =  m_configIni.GetInt(TEXT("Other"), TEXT("Hotkey_Shift"), 1);
	m_bHotKey_Ctrl =  m_configIni.GetInt(TEXT("Other"), TEXT("Hotkey_Ctrl"), 1);
	m_bHotKey_Alt =  m_configIni.GetInt(TEXT("Other"), TEXT("Hotkey_Alt"), 1);
	m_nHotkey_Key =  m_configIni.GetInt(TEXT("Other"), TEXT("Hotkey_Key"), 65);
	m_nSetModifier = 0x0000;
	m_wShowModifier = 0x00;

	//组合得到参数
	if(TRUE == m_bHotKey_Shift)
	{
		m_nSetModifier |= MOD_SHIFT;
		m_wShowModifier |= HOTKEYF_SHIFT;
	}
	if(TRUE == m_bHotKey_Ctrl)
	{
		m_nSetModifier |= MOD_CONTROL;
		m_wShowModifier |= HOTKEYF_CONTROL;
	}
	if(TRUE == m_bHotKey_Alt)
	{
		m_nSetModifier |= MOD_ALT;
		m_wShowModifier |= HOTKEYF_ALT;
	}

	m_bHide = m_configIni.GetInt(TEXT("Other"), TEXT("Hide"), 0);

	/********************初始化操作********************/
	//是否显示到后台运行
	if (TRUE == m_bHide)
	{
		ShowWindow(SW_HIDE);
	}
	else
	{
		//托盘化
		ToTray();
	}

	//显示当前设置到界面
	UpdateData(FALSE);
	m_HotKeyCtrl.SetHotKey(m_nHotkey_Key, m_wShowModifier);

	//保证用户第一次使用时加入开机启动
	if (m_bAutoRun == TRUE)
	{
		SetAutoRun();
	}

	//设置全局热键
	m_atom = GlobalAddAtom(TEXT("PUNCH_JIMWEN"));
	if (FALSE == RegisterHotKey(m_hWnd, m_atom, m_nSetModifier, m_nHotkey_Key))
	{
		MessageBox(TEXT("设置热键失败，可能产生冲突，请换其它热键!"));
	}

	//开机提醒打卡
	if (m_bStartNotify == TRUE)
	{
		MessageBox(TEXT("别着急坐下，打卡了吗，同志!"), TEXT("提醒"), MB_OK | MB_SYSTEMMODAL | MB_ICONWARNING);
	}

	//调整关机顺序和设置关机提醒原因
	SetProcessShutdownParameters(0x4fe, SHUTDOWN_NORETRY);//提升到最先关闭

	//开始计时
	SetTimer(TIMER_1, 1000, NULL);

	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void CPunchDlg::OnNcPaint()
{
	// TODO: 在此处添加消息处理程序代码
	// 不为绘图消息调用 CDialog::OnNcPaint()

	//程序一启动就隐藏对话框,只运行这一次
	static BOOL bISHideWindow = TRUE;
	if (TRUE == bISHideWindow)
	{
		ShowWindow(SW_HIDE);
		bISHideWindow = FALSE;
	}

	CDialog::OnNcPaint();
}

void CPunchDlg::OnDestroy()
{
	CDialog::OnDestroy();

	// TODO: 在此处添加消息处理程序代码

	//退出时强制删除托盘图标,防止出现程序退出托盘图标还在
	DeleteTray();
}

void CPunchDlg::OnSysCommand(UINT m_nid, LPARAM lParam)
{
	if ((m_nid & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(m_nid, lParam);
	}
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CPunchDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CPunchDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

/************************************************************************/
/* 托盘化相关操作                                                       */
/************************************************************************/
BOOL CPunchDlg::ToTray()
{
	//当前已经托盘化了直接表示成功
	if (TRUE == m_bIsToTray)
	{
		return TRUE;
	}

	//设置托盘设置结构体参数
	m_nid.cbSize = sizeof(NOTIFYICONDATA);									//托盘设置结构体大小
	m_nid.hWnd = this->GetSafeHwnd();										//设置图盘图标对应的窗口句柄
	m_nid.uID = IDR_MAINFRAME;												//托盘图标ID
	m_nid.hIcon = LoadIcon(	AfxGetInstanceHandle(), MAKEINTRESOURCE(IDR_MAINFRAME));//托盘图标句柄
	m_nid.uFlags = /*NIF_INFO|*/NIF_ICON|NIF_TIP|NIF_MESSAGE;					//托盘显示气泡，图标，鼠标悬停提示,接受托盘消息	
	m_nid.uCallbackMessage = WM_NOTIFYICON;								//设置操作托盘图标时的消息
	m_nid.uTimeout= 500;													//设置托盘化时的提示信息(m_nid.szInfo)的显示时间
	m_nid.dwInfoFlags = NIIF_USER;											//设置气泡框的图标
	lstrcpy(m_nid.szTip,   TEXT("左键双击显示设置对话框\n右键单击显示菜单"));							//设置鼠标悬停提示
	lstrcpy(m_nid.szInfo,   TEXT("左键双击显示设置对话框,右键单击显示菜单"));							//设置托盘化时的提示信息(m_nid.szInfo)
	lstrcpy(m_nid.szInfoTitle,   TEXT("提示"));								//设置托盘化时的提示信息(m_nid.szInfo)的标题

	//在托盘区添加图标
	if(::Shell_NotifyIcon(NIM_ADD, &m_nid)) 									
	{
		ShowWindow(SW_HIDE);							//创建托盘图标成功则隐藏主窗口
		m_bIsToTray = TRUE;

		return TRUE ;
	}
	else
	{
		MessageBox(TEXT("创建托盘图标失败"), TEXT("错误"), MB_OK) ;
		return FALSE ;
	}
}

BOOL CPunchDlg::DeleteTray()
{
	//当前已经删除托盘化了表示删除成功
	if (FALSE == m_bIsToTray)
	{
		return TRUE;
	}

	//删除托盘图标
	if(::Shell_NotifyIcon(NIM_DELETE, &m_nid)) 									
	{
		m_bIsToTray = FALSE;
		return TRUE ;
	}
	else
	{
		MessageBox(TEXT("删除托盘图标失败"), TEXT("错误"), MB_OK) ;
		return FALSE ;
	}
}

LRESULT CPunchDlg::OnNotifyIcon( WPARAM wParam, LPARAM lParam )
{
	switch(lParam)//根据lParam判断相对应的事件
	{
	case WM_LBUTTONDBLCLK://如果左键双击托盘图标,则显示设置窗体
		OnSetting();
		break;
	case WM_RBUTTONUP://如果右键菜单弹起,则弹出菜单
		CPoint pos;
		GetCursorPos(&pos);
		CMenu menu;
		menu.LoadMenu(IDR_MAIN_MENU);
		CMenu *m_pMenu = menu.GetSubMenu(0);

		if(m_pMenu != NULL)
		{
			SetForegroundWindow();//加这句是为了鼠标点击其他地方时,弹出的菜单能够消失
			m_pMenu->TrackPopupMenu(TPM_RIGHTBUTTON|TPM_RIGHTALIGN, pos.x+1, pos.y+1, this);
		}
		break;
	}

	return 0;
}

/************************************************************************/
/* 托盘菜单响应                                                         */
/************************************************************************/
void CPunchDlg::OnSetting()
{
	// TODO: 在此添加命令处理程序代码

	//显示当前设置到界面
	UpdateData(FALSE);
	m_HotKeyCtrl.SetHotKey(m_nHotkey_Key, m_wShowModifier);

	ShowWindow(SW_SHOWNORMAL);
	SetForegroundWindow();
}

void CPunchDlg::OnHelp()
{
	// TODO: 在此添加命令处理程序代码
	CHelpDlg helpDlg;
	helpDlg.DoModal();
}

void CPunchDlg::OnAbout()
{
	// TODO: 在此添加命令处理程序代码
	CAboutDlg dlgAbout;
	dlgAbout.DoModal();
}

void CPunchDlg::OnExit()
{
	// TODO: 在此添加命令处理程序代码

	//自己new的对话框必须自己Destroy
	DestroyWindow();
}

/************************************************************************/
/* 设置对话框操作                                                       */
/************************************************************************/
void CPunchDlg::OnBnClickedOk()
{
	// TODO: 在此添加控件通知处理程序代码
	//获得当前界面设置参数
	UpdateData(TRUE);
	m_HotKeyCtrl.GetHotKey((WORD &)m_nHotkey_Key, m_wShowModifier);

	//实际响应时间
	m_timeMornAct = m_timeMorn;
	m_timeNoonAct = m_timeNoon;
	m_timeAfterAct = m_timeAfter;
	m_timeNightAct = m_timeNight;

	//实际热键值对应到内存参数
	m_nSetModifier = 0x0000;
	if(m_wShowModifier & HOTKEYF_SHIFT)
	{
		m_bHotKey_Shift = 1;
		m_nSetModifier |= MOD_SHIFT;
	}
	else
	{
		m_bHotKey_Shift = 0;
	}
	if (m_wShowModifier & HOTKEYF_CONTROL)
	{
		m_bHotKey_Ctrl = 1;
		m_nSetModifier |= MOD_CONTROL;
	}
	else
	{
		m_bHotKey_Ctrl = 0;
	}
	if (m_wShowModifier & HOTKEYF_ALT)
	{
		m_bHotKey_Alt = 1;
		m_nSetModifier |= MOD_ALT;
	}
	else
	{
		m_bHotKey_Alt = 0;
	}

	//设置Config文件
	m_configIni.SetInt(TEXT("Morning"), TEXT("Enable"), m_bMornEnable);
	m_configIni.SetInt(TEXT("Morning"), TEXT("Hour"), m_timeMorn.GetHour());
	m_configIni.SetInt(TEXT("Morning"), TEXT("Minute"), m_timeMorn.GetMinute());
	m_configIni.SetInt(TEXT("Morning"), TEXT("Second"), m_timeMorn.GetSecond());
	m_configIni.SetInt(TEXT("Morning"), TEXT("Space"), m_nMornSpace);

	m_configIni.SetInt(TEXT("Noon"), TEXT("Enable"), m_bNoonEnable);
	m_configIni.SetInt(TEXT("Noon"), TEXT("Hour"), m_timeNoon.GetHour());
	m_configIni.SetInt(TEXT("Noon"), TEXT("Minute"), m_timeNoon.GetMinute());
	m_configIni.SetInt(TEXT("Noon"), TEXT("Second"), m_timeNoon.GetSecond());
	m_configIni.SetInt(TEXT("Noon"), TEXT("Space"), m_nNoonSpace);

	m_configIni.SetInt(TEXT("Afternoon"), TEXT("Enable"), m_bAfterEnable);
	m_configIni.SetInt(TEXT("Afternoon"), TEXT("Hour"), m_timeAfter.GetHour());
	m_configIni.SetInt(TEXT("Afternoon"), TEXT("Minute"), m_timeAfter.GetMinute());
	m_configIni.SetInt(TEXT("Afternoon"), TEXT("Second"), m_timeAfter.GetSecond());
	m_configIni.SetInt(TEXT("Afternoon"), TEXT("Space"), m_nAfterSpace);

	m_configIni.SetInt(TEXT("Night"), TEXT("Enable"), m_bNightEnable);
	m_configIni.SetInt(TEXT("Night"), TEXT("Hour"), m_timeNight.GetHour());
	m_configIni.SetInt(TEXT("Night"), TEXT("Minute"), m_timeNight.GetMinute());
	m_configIni.SetInt(TEXT("Night"), TEXT("Second"), m_timeNight.GetSecond());
	m_configIni.SetInt(TEXT("Night"), TEXT("Space"), m_nNightSpace);

	m_configIni.SetInt(TEXT("Other"), TEXT("Autorun"), m_bAutoRun);
	m_configIni.SetInt(TEXT("Other"), TEXT("StartNotify"), m_bStartNotify);
	m_configIni.SetInt(TEXT("Other"), TEXT("EndNotify"), m_bEndNotify);

	m_configIni.SetInt(TEXT("Other"), TEXT("Hotkey_Shift"), m_bHotKey_Shift);
	m_configIni.SetInt(TEXT("Other"), TEXT("Hotkey_Ctrl"), m_bHotKey_Ctrl);
	m_configIni.SetInt(TEXT("Other"), TEXT("Hotkey_Alt"), m_bHotKey_Alt);
	m_configIni.SetInt(TEXT("Other"), TEXT("Hotkey_Key"), m_nHotkey_Key);

	m_configIni.SetInt(TEXT("Other"), TEXT("Hide"), m_bHide);


	//处理开机启动项
	if (TRUE == m_bAutoRun)
	{
		SetAutoRun();
	}
	else
	{
		CancelAutoRun();
	}

	//设置全局热键并保存到文件
	UnregisterHotKey(m_hWnd, m_atom);
	if (FALSE == RegisterHotKey(m_hWnd, m_atom, m_nSetModifier, m_nHotkey_Key))
	{
		MessageBox(TEXT("设置热键失败，可能产生冲突，请换其它热键!"));
	}
	else
	{
		ShowWindow(SW_HIDE);
	}

	//是否显示到后台运行
	if (TRUE == m_bHide)
	{
		DeleteTray();
		ShowWindow(SW_HIDE);
	}
	else
	{
		//托盘化
		ToTray();
	}
}

void CPunchDlg::OnBnClickedCancel()
{
	// TODO: 在此添加控件通知处理程序代码
	ShowWindow(SW_HIDE);
}

void CPunchDlg::OnBnClickedStartNotify()
{
	// TODO: 在此添加控件通知处理程序代码
	if (IsDlgButtonChecked(IDC_START_NOTIFY))
	{
		CheckDlgButton(IDC_AUTORUN, TRUE);//开启开机提醒时一定设置开机启动
	}
}

void CPunchDlg::OnBnClickedAutorun()
{
	// TODO: 在此添加控件通知处理程序代码
	if (FALSE == IsDlgButtonChecked(IDC_AUTORUN))
	{
		CheckDlgButton(IDC_START_NOTIFY, FALSE);//取消自动启动时一定取消开机提醒
	}
}

void CPunchDlg::OnHotKey(UINT nHotKeyId, UINT nKey1, UINT nKey2)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	OnSetting();//响应热键时显示窗口

	CDialog::OnHotKey(nHotKeyId, nKey1, nKey2);
}

/************************************************************************/
/* 定时到达后操作                                                       */
/************************************************************************/
void CPunchDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	if (TIMER_1 == nIDEvent)
	{
		CTime curTime = CTime::GetCurrentTime();

		if (TRUE == m_bMornEnable)
		{
			if (curTime == m_timeMornAct)
			{
				if (IDNO == MessageBox(TEXT("早上打卡了吗，同志!\n\n已经打卡选择<是>\n等一下再提示选择<否>"), NULL, MB_YESNO | MB_SYSTEMMODAL | MB_ICONWARNING))
				{
					m_timeMornAct = m_timeMornAct + CTimeSpan(0, 0, m_nMornSpace, 0);
				}
			}
		}
		if (TRUE == m_bNoonEnable)
		{
			if (curTime == m_timeNoonAct)
			{
				if (IDNO == MessageBox(TEXT("中午打卡了吗，同志!\n\n已经打卡选择<是>\n等一下再提示选择<否>"), NULL, MB_YESNO | MB_SYSTEMMODAL | MB_ICONWARNING))
				{
					m_timeNoonAct = m_timeNoonAct + CTimeSpan(0, 0, m_nNoonSpace, 0);
				}
			}
		}
		if (TRUE == m_bAfterEnable)
		{
			if (curTime == m_timeAfterAct)
			{
				if (IDNO == MessageBox(TEXT("下午打卡了吗，同志!\n\n已经打卡选择<是>\n等一下再提示选择<否>"), NULL, MB_YESNO | MB_SYSTEMMODAL | MB_ICONWARNING))
				{
					m_timeAfterAct = m_timeAfterAct + CTimeSpan(0, 0, m_nAfterSpace, 0);
				}
			}
		}
		if (TRUE == m_bNightEnable)
		{
			if (curTime == m_timeNightAct)
			{
				if (IDNO == MessageBox(TEXT("晚上打卡了吗，同志!\n\n已经打卡选择<是>\n等一下再提示选择<否>"), NULL, MB_YESNO | MB_SYSTEMMODAL | MB_ICONWARNING))
				{
					m_timeNightAct = m_timeNightAct + CTimeSpan(0, 0, m_nNightSpace, 0);
				}
			}
		}
	}

	CDialog::OnTimer(nIDEvent);
}


/************************************************************************/
/* 开机自动启动操作                                                     */
/************************************************************************/
BOOL CPunchDlg::SetAutoRun()
{
	CRegKey regKey;
	if(regKey.Open(HKEY_LOCAL_MACHINE, TEXT("Software\\Microsoft\\Windows\\CurrentVersion\\Run")) == ERROR_SUCCESS)
	{
		if (ERROR_SUCCESS == regKey.SetValue(TEXT("Jimwen-Punch"), REG_SZ, theApp.m_csExeFileName.GetBuffer(256), (theApp.m_csExeFileName.GetLength()+1)*sizeof(TCHAR)))//注意这里最后一个参数是nBits,且要求size算上terminating null character
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}

		regKey.Close();
	}
	else
	{
		return FALSE;
	}
}

BOOL CPunchDlg::CancelAutoRun()
{
	CRegKey regKey;
	if(regKey.Open(HKEY_LOCAL_MACHINE, TEXT("Software\\Microsoft\\Windows\\CurrentVersion\\Run")) == ERROR_SUCCESS)
	{
		if (ERROR_SUCCESS == regKey.DeleteValue(TEXT("Jimwen-Punch")))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}

		regKey.Close();
	}
	else
	{
		return FALSE;
	}
}

/************************************************************************/
/* 关机提醒操作                                                         */
/************************************************************************/
BOOL CPunchDlg::OnQueryEndSession()
{
	if (!CDialog::OnQueryEndSession())
		return FALSE;

	// TODO:  在此添加专用的查询结束会话代码
	if (m_bEndNotify == TRUE)
	{
		MessageBox(TEXT("别着急走，打卡了吗，最后一次机会哟!"), TEXT("提醒"), MB_OK | MB_SYSTEMMODAL | MB_ICONWARNING);
	}

	return TRUE;
}
