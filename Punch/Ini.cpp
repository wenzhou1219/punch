/********************************************************************
	日期:	2013/12/03
	文件: 	Ini.cpp Ini.h
	作者:	文洲
	版本:	V1.0
	
	功能:	对Ini文件的操作做一个简单的封装
	说明:	无
*********************************************************************/

#include "stdafx.h"
#include "Ini.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

/**
 *功能:默认构造函数，默认操作WIN.ini文件
 *参数:无
 *返回:无
 *其他:2013/12/03 By 文洲 Ver1.0
**/
CIni::CIni()
{
	m_csFileName = TEXT("WIN.ini");
}

/**
 *功能:带参数构造函数,传入参数必须是带full path的路径,
	   例如当前目录下1.ini文件传入时必须写成.\\1.ini,
	   否则Windows会默认搜索Windows文件夹
 *参数:szFileName--带完整文件路径的ini文件名
 *返回:无
 *其他:2013/12/03 By 文洲 Ver1.0
**/
CIni::CIni( LPCTSTR szFileName )
{
	m_csFileName = szFileName;
}

CIni::~CIni()
{

}

/**
 *功能:设置Section的值,注意Section原来的值会被删除,重新置为传入的值
 *参数:lpAppName--section名
	   lpString--key=value形式的键值对,相邻之间用\0隔开,最后以\0\0收尾
 *返回:成功为TRUE,失败为FALSE
 *其他:2013/12/03 By 文洲 Ver1.0
 *示例:
 *原来文件内容为:
 [section1]
 key1=1
 key2=2
 *调用函数
 SetSection("section1", "key3=3\0key4=4\0\0")
 *后,文件内容为:
 [section1]
 key3=3
 key4=4
**/
BOOL CIni::SetSection( LPCTSTR lpAppName, LPCTSTR lpString )
{
	return WritePrivateProfileSection(lpAppName, lpString, m_csFileName.GetBuffer(256));
}

/**
 *功能:设置Section下的Key的string值
 *参数:lpAppName--section名
	   lpKeyName--Key名
	   lpString--以\0结尾的String的值
 *返回:成功为TRUE,失败为FALSE
 *其他:2013/12/03 By 文洲 Ver1.0
**/
BOOL CIni::SetString( LPCTSTR lpAppName, LPCTSTR lpKeyName, LPCTSTR lpString )
{
	return WritePrivateProfileString(lpAppName, lpKeyName, lpString, m_csFileName.GetBuffer(256));
}

BOOL CIni::SetStruct( LPCTSTR lpszSection, LPCTSTR lpszKey, LPVOID lpStruct, UINT uSizeStruct )
{
	return WritePrivateProfileStruct(lpszSection, lpszKey, lpStruct, uSizeStruct, m_csFileName.GetBuffer(256));
}

/**
 *功能:设置Section下的Key的int值
 *参数:lpAppName--section名
	   lpKeyName--Key名
	   nValue--int值
 *返回:成功为TRUE,失败为FALSE
 *其他:2013/12/03 By 文洲 Ver1.0
**/
BOOL CIni::SetInt( LPCTSTR lpAppName, LPCTSTR lpKeyName, const INT nValue )
{
	CString csTemp;
	csTemp.Format(TEXT("%d"), nValue);

	return SetString(lpAppName, lpKeyName, csTemp.GetBuffer(256));
}

/**
 *功能:设置Section下的Key的double值
 *参数:lpAppName--section名
	   lpKeyName--Key名
	   dbValue--double值
 *返回:成功为TRUE,失败为FALSE
 *其他:2013/12/03 By 文洲 Ver1.0
**/
BOOL CIni::SetDouble( LPCTSTR lpAppName, LPCTSTR lpKeyName, const DOUBLE dbValue )
{
	CString csTemp;
	csTemp.Format(TEXT("%.3f"), dbValue);
	
	return SetString(lpAppName, lpKeyName, csTemp.GetBuffer(256));
}

/**
 *功能:获得Section的值
 *参数:lpAppName--section名
 *返回:key=value形式的键值对,相邻之间用\n隔开,最后以\n收尾
 *其他:2013/12/03 By 文洲 Ver1.0
 *示例:
 *原来文件内容为:
 [section1]
 key3=3
 key4=4
 *调用函数
 GetSection("section1")
 *后,返回内容为:
 "key3=3\nkey4=4\n"
**/
CString CIni::GetSection( LPCTSTR lpAppName )
{
	TCHAR szBuffer[256];

	//注意这个函数返回key=value形式的键值对,相邻之间用\0隔开,最后以\0\0收尾
	/*示例:
		原来文件内容为:
		[section1]
		key3=3
		key4=4
		*调用函数
		GetPrivateProfileSection("section1",...)
		后,返回内容为:
		"key3=3\0key4=4\0\0"
	*/
	GetPrivateProfileSection(lpAppName, szBuffer, 256, m_csFileName.GetBuffer(256));

	//将得到的字符串处理成以\n分割的键值对,主要是操作指针
	TCHAR szTemp[256];
	int lengthTemp;
	TCHAR *szPointDest = szBuffer;
	CString csTemp;
	while(1)
	{
		lstrcpy(szTemp, szPointDest);
		lengthTemp = lstrlen(szTemp);
		
		if (lengthTemp > 0)
		{
			csTemp += szTemp;
			csTemp += "\n";
			szPointDest += lengthTemp+1;//跳转到下一个\0后
		}
		else
		{
			break;
		}
	}

	return csTemp;
}

/**
 *功能:获得所有的Section
 *参数:无
 *返回:相邻之间用\n隔开的section名,最后以\n收尾
 *其他:2013/12/03 By 文洲 Ver1.0
 *示例:
 *原来文件内容为:
 [section1]
 key3=3
 [section2]
 key4=4
 *调用函数
 GetSectionNames()
 *后,返回内容为:
 "section1\nsection2\n"
**/
CString CIni::GetSectionNames()
{
	TCHAR szBuffer[256];
	GetPrivateProfileSectionNames(szBuffer, 256, m_csFileName.GetBuffer(256));
	
	//将得到的字符串处理成以\n分割的键值对,主要是操作指针
	TCHAR szTemp[256];
	int lengthTemp;
	TCHAR *szPointDest = szBuffer;
	CString csTemp;
	while(1)
	{
		lstrcpy(szTemp, szPointDest);
		lengthTemp = lstrlen(szTemp);
		
		if (lengthTemp > 0)
		{
			csTemp += szTemp;
			csTemp += "\n";
			szPointDest += lengthTemp+1;//跳转到下一个\0后
		}
		else
		{
			break;
		}
	}
	
	return csTemp;
}

/**
 *功能:获得Section下Key的String值
 *参数:lpAppName--section名
	   lpKeyName--key名
	   lpDefault--默认default值
 *返回:对应的String值
 *其他:2013/12/03 By 文洲 Ver1.0
**/
CString CIni::GetString( LPCTSTR lpAppName, LPCTSTR lpKeyName, LPCTSTR lpDefault )
{
	TCHAR szBuffer[256];
	GetPrivateProfileString(lpAppName, lpKeyName, lpDefault, szBuffer, 256, m_csFileName.GetBuffer(256));
	
	return szBuffer;
}

BOOL CIni::GetStruct( LPCTSTR lpszSection, LPCTSTR lpszKey, LPVOID lpStruct, UINT uSizeStruct )
{
	return GetPrivateProfileStruct(lpszSection, lpszKey, lpStruct, uSizeStruct, m_csFileName.GetBuffer(256));
}

/**
 *功能:获得Section下Key的int值
 *参数:lpAppName--section名
	   lpKeyName--key名
	   nDefault--默认default值
 *返回:对应的int值
 *其他:2013/12/03 By 文洲 Ver1.0
**/
INT CIni::GetInt( LPCTSTR lpAppName, LPCTSTR lpKeyName, const INT nDefault )
{
	TCHAR szBuffer[256];
	CString csTemp;
	csTemp.Format(TEXT("%d"), nDefault);
	GetPrivateProfileString(lpAppName, lpKeyName, csTemp.GetBuffer(256), szBuffer, 256, m_csFileName.GetBuffer(256));
	
	return  _wtoi(szBuffer);//注意这里使用_wtoi而不是atoi
}

/**
 *功能:获得Section下Key的double值
 *参数:lpAppName--section名
	   lpKeyName--key名
	   dbDefault--默认default值
 *返回:对应的double值
 *其他:2013/12/03 By 文洲 Ver1.0
**/
DOUBLE CIni::GetDouble( LPCTSTR lpAppName, LPCTSTR lpKeyName, const DOUBLE dbDefault )
{
	TCHAR szBuffer[256];
	CString csTemp;
	csTemp.Format(TEXT("%f"), dbDefault);
	GetPrivateProfileString(lpAppName, lpKeyName, csTemp.GetBuffer(256), szBuffer, 256, m_csFileName.GetBuffer(256));
	
	return _wtof(szBuffer);
}
