//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Punch.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDS_HELPTEXT                    102
#define IDD_ALERT                       102
#define IDD_HELP                        102
#define IDD_SETTING                     104
#define IDR_MAINFRAME                   129
#define IDR_MAIN_MENU                   130
#define IDE_HELP                        1000
#define IDC_AUTORUN                     1001
#define IDC_START_NOTIFY                1002
#define IDC_AUTORUN3                    1003
#define IDC_END_NOTIFY                  1003
#define IDB_HIDE                        1004
#define IDC_HOTKEY1                     1005
#define IDC_HOTKEY                      1005
#define IDB_MORNING_TIME                1016
#define IDB_NOON_TIME                   1017
#define IDB_MORNING_SPACE               1018
#define IDB_NOON_SPACE                  1019
#define IDB_AFTERNOON_TIME              1020
#define IDB_AFTERNOON_SPACE             1021
#define IDB_NIGHT_TIME                  1022
#define IDB_NIGHT_SPACE                 1023
#define IDB_MORNING_ENABLE              1025
#define IDB_NOON_ENABLE                 1026
#define IDB_AFTERNOON_ENABLE            1027
#define IDC_WEBSITE_LINK                1027
#define IDB_NIGHT_ENABLE                1028
#define ID_CESHI_CESHI2                 32771
#define ID_CESHI_CESHI3                 32772
#define ID_32773                        32773
#define ID_32774                        32774
#define IDM_SETTING                     32775
#define IDM_HELP                        32776
#define IDM_ABOUT                       32777
#define IDM_EXIT                        32778

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        131
#define _APS_NEXT_COMMAND_VALUE         32779
#define _APS_NEXT_CONTROL_VALUE         1006
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
